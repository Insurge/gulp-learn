module.exports = function () {
    $.gulp.task('img:dev', function () {
        return $.gulp.src('src/static/img/*.{png,jpg,gif}')
            .pipe($.gulp.dest('build/static/img/'));
    });

    $.gulp.task('img:build', function () {
        return $.gulp.src('src/static/img/*.{png,jpg,gif}')
            .pipe($.gp.tinypng('L0VUmduSSlly8rE3Y4cBP7Q_F5-DUWMy'))
            .pipe($.gulp.dest('build/static/img/'));
    });
};